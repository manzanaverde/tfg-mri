const ObjectId = require('mongodb').ObjectId; 

class Project {
    constructor(server) {
        this.server = server;
        this.collectionName = "projects";
    }

    bindRequests(express) {
        express.post("/project/create", this.server.upload.fields([{ name: "files"}, { name: "vol", maxCount: 1}]), this.createProject.bind(this));
        express.post("/project/get/byId", this.server.upload.none(), this.getProjectById.bind(this));
        express.post("/project/get/scene", this.server.upload.none(), this.getProjectScene.bind(this));
        express.post("/project/update/scene", this.server.upload.none(), this.updateProjectScene.bind(this));
        express.post("/project/get/volume", this.server.upload.none(), this.getProjectVolume.bind(this));
        express.get("/projects/get/all", this.server.upload.none(), this.getAllProjects.bind(this));
        express.post("/projects/get/byQuery", this.server.upload.none(), this.getProjectsByQuery.bind(this));
    }

    async createProject(req, res) {
        try {
            
            // At this point, files are already in the project directory, but all mixed. We need to place them in the correct subfolder
            const projectName = req.body.projectName;
            const patientId = req.body.patientId;
            const projectFolder = "server/projects/" + projectName;

            const scansFolder = projectName + "/scans";
            const dicomFolder = projectName + "/scans/dicom";
            const volFolder = projectName + "/scans/vol";

            // Create necessary subfolders
            await this.server.fs.createFolder(scansFolder);
            await this.server.fs.createFolder(dicomFolder);
            await this.server.fs.createFolder(volFolder);

            const dirFiles = await this.server.fs.getProjectFileNames(projectName, true);

            // Move each file to the corresponding subfolder
            for (let f = 0; f < dirFiles.length; f++) {

                let file = dirFiles[f];

                if (!file.isFile())     // Skip directories
                    continue;
                
                let src = projectFolder + "/" + file.name;
                let dst = null;

                if (file.name.includes("scene.json"))
                    continue;
                else if (file.name.endsWith(".dcm"))
                    dst = dicomFolder;
                else if (file.name.endsWith(".vl"))
                    dst = volFolder;
                else
                    dst = projectFolder;

                dst += "/" + file.name;
                    
                await this.server.fs.copyFile(src, "server/projects/" + dst);
                await this.server.fs.removeFile(src);
            }
        

            // Create and upload scene.json
            const params = {
                lastId: 1,               
                sceneItems:[
                    { id: 1, "type": "volumen", "folderName": "/scans/" }
                ],
                views: {
                }
            }
            this.server.fs.createJSONScene(JSON.stringify(params), projectName);
            await this.saveProjectToDB(projectName, patientId);
            res.end(`Project ${projectName} created successfully`);
        }
        catch(error) {
            console.log(`Error trying to create project: ${error}`);
        }
    }

    saveProjectToDB(projectName, patientId) {
        const project = 
        {
            name: projectName,
            patientId: patientId,
            folder: projectName,
            creationTs: Date.now() 
        }
        this.insertProject(project);
    }

    insertProject(project) {
        this.server.db.insertDocument(project, this.collectionName);
    }

    /**
     * 
     * @param {Array} projects Array of objects
     */
    insertProjects(projects) {
        this.server.db.insertDocuments(projects, this.collectionName);
    }

    async getAllProjects(req, res) {
        const all = await this.server.db.getAllDocuments(this.collectionName);
        res.end(JSON.stringify(all));
    }

    async getProjectsByQuery(req, res) {
        
        if (!req.body) {
            res.end("The request has no body");
            return;
        }

        const by = req.body.searchBy;
        const value = req.body.userQuery;
        const query = { [by]: value };
        const results = await this.server.db.getDocumentsByQuery(query, this.collectionName);
        res.end(JSON.stringify(results));
    }

    async getProjectById(req, res) {
        try {
            if (!req.body) {
                res.end("The request has no body");
                return;
            }
    
            const id = req.body.projectId;
            const objId = new ObjectId(id);
            const project = await this.server.db.getDocumentByQuery( { _id: objId }, this.collectionName);
            res.end(JSON.stringify(project));
        }
        catch(error) {
            res.status(404).send("Project not found");
        }
    }       

    getProjectScene(req, res) {
        try {
            const folder = req.body.folder;
            const options = { 
                root: "/home/anna/TFG/tfg-mri/server/projects/" + folder + "/",
                headers: { "Content-type": 'application/octet-stream' } 
            };
            const scene = `scene.json`;
           
            res.sendFile(scene, options, function (err) {

                if (err) {
                    //res.sendStatus(404).send(err);
                } else {
                    //res.sendStatus(200).send("Scene.json sent");
                }
            });
        }
        catch(error) {
            res.end(JSON.stringify(error));
        }
    }

    // store new scene.json in the server
    async updateProjectScene(req, res) {
        try {
            const scene = JSON.parse(req.body.scene);
            const objId = new ObjectId(req.body.projectId);
            const project = await this.server.db.getDocumentByQuery( { _id: objId }, this.collectionName);

            this.server.fs.createJSONScene(scene, project.name);            
        }
        catch(error) {
            res.end(JSON.stringify(error));
        }
    }

    getProjectVolume(req, res) {
        try {
            const folder = req.body.folder;

            if (!folder) {
                res.end("Folder field was not received");
                return;
            }

            const options = { root: "server/projects/" + folder + "/" }
            const vol = `scans/vol/data.vl`;
           
            res.sendFile(vol, options, function (err) {

                if (err) {
                    console.log(err);
                } else {
                    res.end("File sent");
                }
            });
        }
        catch(error) {
            res.end(JSON.stringify(error));;
        }
    }

}

module.exports = Project;