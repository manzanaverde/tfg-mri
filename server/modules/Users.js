const crypto = require("crypto");

class Users {

    constructor(server) {
        this.server = server;
        this.collectionName = "users";
    }

    bindRequests(express) {
        express.get("/users/get/all", this.server.upload.none(), this.getAllUsers.bind(this));
        express.post("/users/register", this.server.upload.none(), this.register.bind(this));
        express.get("/users/login", this.login.bind(this));
        express.get("/users/logout", this.logout.bind(this));
        express.post("/users/delete", this.server.upload.none(), this.deleteUser.bind(this));
    }


    async getAllUsers(req, res) {
        try {
            const users = await this.server.db.getAllDocuments(this.collectionName);
            res.end(JSON.stringify(users));
        }
        catch(error) {
            console.error(error);
        }
    }

    async register(req, res) {
        try {
            const user = this.createUser(req.body);

            if (!user)
                res.status(404).send("Could not create user");

            const result = await this.server.db.insertDocument(user, this.collectionName);
            res.status(200).send( { code: 200, msg: `User has been created. You can login with your credentials` });
        }
        catch(error) {
            res.status(500).send(error);
        }
    }


    createUser(params) {

        if (params.password != params.confirmPassword)
            return null;

        // A pseudo-random salt will be used to create a hash
        const salt = crypto.randomBytes(16).toString("hex");

        return {
            firstName: params.firstName,
            lastName: params.lastName,
            email: params.email,      // Will be used as ID
            password: this.createHashedPass(params.password, salt),
            job: params.job,
            organization: params.organization,
            organizationCity: params.organizationCity,
            bio: params.bio,
            salt: salt
        }
    }

    async login(req, res) {
        try {
            
            // Login form info
            const email = req.query.email;
            const password = req.query.pass;
            
            // Fetch user from DB
            const query = { email: email };
            const user = await this.server.db.getDocumentByQuery(query, this.collectionName);

            if (!user) {
                res.status(404).send(`User does not exist`);
                return;
            }
            
            // Confirm if password is correct
            const hashedPwd = this.createHashedPass(password, user.salt);
            if (hashedPwd !== user.password) {
                res.status(501).send(`Email or password incorrect`);
                return;
            }

            // Session stuff
            req.session.userId = user._id;

            const info = {
                "firstName": user.firstName,
                "lastName": user.lastName
            }

            //res.redirect("searchEngine.html");
            res.send(JSON.stringify(info));
        }
        catch(error) {
            console.error(error);
        }
    }

    logout(req, res, next) {
        
        if (req.session) {
            req.session.destroy( (err) => {
                if (err)
                    return next(err);
                else
                    return res.redirect("/");
            });
        }
    }

    static requiresLogin(req, res, next) {
        if (req.session && req.session.userId)
            return next();
        else {
            res.status(401).send("Page only available for registered users");
            return next();
        }
    }

    async deleteUser(req, res) {
        try {
            const email = req.body.email;
            const query = { email: email };
            const result = await this.collection.removeDocument(query);
            res.end(`User removed`);
        }
        catch(error) {
            console.log(error);
        }
    }

    /**
     * Hash the password with SHA-256 algorithm and 1000 iterations
     * @param {String} password 
     * @param {String} salt 
     */
    createHashedPass(password, salt) {
        return crypto.pbkdf2Sync(password, salt, 1000, 64, "sha256").toString("hex");
    }
}

module.exports = Users;