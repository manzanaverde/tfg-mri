class Patients {

    constructor(server) {
        this.server = server;
        this.collectionName = "patients";
    }

    bindRequests(express) {
        express.post("/patients/get/byQuery", this.server.upload.none(), this.getPatientsByQuery.bind(this));
        express.post("/patient/get/byId", this.server.upload.none(), this.getPatientById.bind(this));
        /*express.get("/patients/add", this.addPatient.bind(this));
        express.get("/patients/addMultiple", this.addPatients.bind(this));
        express.get("/patients/all", this.getAllPatients.bind(this));
        express.get("/patients/removeAll", this.removeAllPatients.bind(this));*/
    }

    async getPatientById(req, res) {
        try {
            const patientId = parseInt(req.body.patientId);
            const query = { patientId: patientId };
            const patient = await this.server.db.getDocumentByQuery(query, this.collectionName);
            res.end(JSON.stringify(patient));
        }
        catch(error) {
            console.log(error);
        }
    }

    async getPatientsByQuery(req, res) {
        try {
            const query = { [req.body.searchBy]: req.body.userQuery };
            const patients = await this.server.db.getDocumentsByQuery(query, this.collectionName);
            res.end(JSON.stringify(patients));
        }
        catch(error) {
            console.log(error);
        }
    }



    

    //old format
    async addPatient(req, res) {
        try {
            const patient = req.query("patient");
            const result = await this.collection.insertDocument(patient);

            res.end(`${result.result.n} patients inserted to the DB`);
        }
        catch(error) {
            console.log(error);
        }
    }

    //old format
    async addPatients(req, res) {
        try {
            const patients = req.query("patients");
            const result = await this.collection.insertDocuments(patients);

            res.end(`${result.result.n} patients inserted to the DB`);
        }
        catch(error) {
            console.log(error);
        }
    }

    //old format
    async getAllPatients(req, res) {
        try {
            const patients = await this.collection.getAllDocuments();
            res.end(JSON.stringify(patients));
        }
        catch(error) {
            console.log(error);
        }
    }

    //old format
    async removeAllPatients(req, res) {
        try {
            const result = await this.collection.removeAllDocumentsFromCollection();
            res.end(`Removed ${result.deletedCount} patients`);
        }
        catch(error) {
            console.log(error);
        }
    }


}


class Patient {
    
    constructor(id, name, birthDate, sex, phone = null, mris = [], dependent = false, emergencyContact = null, surgeries = [], comments = []) {
        this.id = id;
        this.name = name;
        this.birthDate = birthDate;
        this.sex = sex;
        this.phone = phone;
        this.mris = mris;
        this.dependent = dependent;
        this.emergencyContact = emergencyContact;
        this.surgeries = surgeries;
        this.comments = comments;
    }

    getObject() {
        return {
            id: this.id,
            name: this.name,
            birthDate: this.birthDate,
            sex: this.sex,
            phone: this.phone,
            mris: this.mris,
            dependent: this.dependent,
            emergencyContact: this.emergencyContact,
            surgeries: this.surgeries,
            comments: this.comments
        }
    }

    addMRI(mri) {
        this.mris.push(mri);
    }

    addSurgery(surgery) {
        this.surgeries.push(surgery);
    }

    addComment(comment) {
        this.comments.push(comment);
    }

}



class MRI {

    constructor(id, date = new Date(), reason, diagnosis = null, filePath = null, edits = null) {
        this.id = id;
        this.date = date;
        this.reason = reason;
        this.diagnosis = diagnosis;
        this.filePath = filePath;
        this.edits = edits;
    }

    getObject() {
        return {
            _id: this.id,
            date: this.date,
            reason: this.reason,
            diagnosis: this.diagnosis,
            filePath: this.filePath,
            edits: this.edits
        }
    }
}

class Surgery {

    constructor(id, date, organ, reason, pronostic, medication = []) {
        this.id = id;
        this.date = date;
        this.organ = organ;
        this.reason = reason;
        this.pronostic = pronostic;
        this.medication = medication;
    }

    getObject() {
        return {
            _id: this.id,
            date: this.date,
            organ: this.organ,
            reason: this.reason,
            pronostic: this.pronostic,
            medication: this.medication
        }
    }
}

module.exports = {
    Patients: Patients,
    Patient: Patient,
    MRI: MRI,
    Surgery: Surgery
}
