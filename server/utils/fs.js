const fs = require('fs');
const fsPromises = fs.promises;

class FS {

    constructor(server) {
        this.projectsPath = "server/projects/";
        this.uploadsPath = "server/uploads";
        this.server = server;
    }

    bindRequests(express) {
        express.get("/download", this.server.upload.none(), this.downloadFile.bind(this));
    }

    createFolder(folderName) {
        fs.mkdir(`${this.projectsPath}${folderName}`, { recursive: true }, err => console.log(err));
    }

    createFile(filePath, fileContent) {
        // Parse fileName for security
        if (filePath.includes("../"))
            console.log(`Unable to write file ${filePath}`);

        fs.writeFile(filePath, fileContent, "utf8", err => {
            if (err) console.log("Error: " + err);
            console.log(`File ${filePath} created`);
        });
    }

    createScan(scan, projectName) {
        const filePath = this.projectsPath + projectName + "/scans/" + scan.originalname;
        this.createFile(filePath, scan);
    }

    createJSONScene(fileContent, projectName) {
        const filePath = this.projectsPath + projectName + "/scene.json";
        this.createFile(filePath, fileContent);
    }

    readFile(filePath, options = null) {
        return new Promise(async (resolve, reject) => {
            try {
                const fileHandle = await fsPromises.open(filePath, "r");
                const fileContent = await fileHandle.readFile(options);
                resolve(fileContent);
            }
            catch(error) {
                reject(error);
            }
        });
    }


    // uses promises!!
    copyFile(source, destination) {
        return fsPromises.copyFile(source, destination);
    }

    // promises
    removeFile(filePath) {
        return fsPromises.unlink(filePath);
    }

    async emptyUploadsFolder() {

        // Get all files from folder
        const files = await fsPromises.readdir(this.uploadsPath);
        const promises = [];
        for (let f = 0; f < files.length; f++) {
            promises.push(this.removeFile(this.uploadsPath + "/" + files[f]));
        } 
        return Promise.all(promises);
    }
/*
    // devuelve solo el nombre del fichero
    getUploadedDicoms() {
        return fsPromises.readdir(this.uploadsPath);
    }*/

    getProjectFileNames(projectName, withFileTypes = false) {
        return fsPromises.readdir(this.projectsPath + "/" + projectName, { withFileTypes: withFileTypes } );
    }
    
    downloadFile(req, res) {
        const fileName = req.body.fileName;
        res.download(fileName, "data.json", function(error) {
            if (error) 
                console.error(error);
            else 
                console.warn("file downloaded");
        })
    }
}

module.exports = FS;