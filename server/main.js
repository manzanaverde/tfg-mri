const webServer = require("./webServer.js");
//const MRI = require("./modules/Patients.js").MRI;

const serverPort = 3000;
//const numPatients = 20;


const createPatients = function() {

    let patients = [];


    for (let i = 0; i < numPatients; i++) {
        let mri = new MRI(i, new Date(), "suspicious blood analysis", "everything OK", null, null).getObject();
        let mris = [ mri ];
        //mris.push(mri);
        patients.push(new Patient(i, `Anna${i}`, "19920810", "female", "666999333", mris, false, "666777888", null, [ "comment1"]).getObject());
    }

    return patients;
}

const printDocs = function(docs) {
    console.log("All docs: ");
    for (let d = 0; d < docs.length; d++) { 
        console.log(`${docs[d].name} ${docs[d].date}`);
    }
};


async function main() {
    try {
        const server = new webServer(serverPort);
        await server.start();
    }
    catch(error) {
        console.log(error);
    }
}


main();