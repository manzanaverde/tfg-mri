#!/bin/bash

# start db server
sudo mongod --dbpath=data &

# start webserver
node --inspect-brk main.js &

