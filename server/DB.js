const MongoClient = require('mongodb').MongoClient;

class MongoDB {

    constructor(port, dbName) {
        const url =  `mongodb://localhost:${port}/${dbName}`;

        this.client = new MongoClient(url, { useNewUrlParser: true } );
        this.db = null;     // Reference to the actual DB
        this.dbName = dbName;
    }

    /**
     * Connects to the db and stores the collection for future use
     */
    async connect() {
        return new Promise(async (resolve, reject) => {
            try {
                await this.client.connect(); 
                console.log("Connected successfully to server");
    
                this.db = this.client.db();
                resolve();
            }
            catch(error) {
                reject(error);
            }
        });
    }

    /**
     * Closes the connection to the database
     */
    disconnect() {
        this.client.close();
    }

    /**
     * Inserts a single document to the database and returns a promise that provides a result. 
     * The result.insertedId promise contains the _id of the newly inserted document.
     * @param {Object} document 
     * @param {String} collectionName 
     */
    insertDocument(document, collectionName) {

        if (!document || !collectionName) 
            return Promise.reject("ERROR: no document or collection where to insert to");

        return this.db.collection(collectionName).insertOne(document);
    }

    /**
     * Inserts multiple documents to the database and returns a promise that provides a result. 
     * The result.insertedIds field contains an array with the _id of each newly inserted document.
     * @param {Array} documents Array of objects 
     * @param {String} collectionName 
     */
    insertDocuments(documents, collectionName) {

        if (!documents || documents.length == 0 || !collectionName)
            return Promise.reject("ERROR: no documents or collection to insert");

        return this.db.collection(collectionName).insertMany(documents);
    }

    /**
     * Fetches all documents from the DB collection and returns an array with the documents
     * @param {String} collectionName 
     */
    getAllDocuments(collectionName) {
        return this.db.collection(collectionName).find({}).toArray();
    }

    /**
     * Returns an array with all documents that fulfill the query
     * @param {Object} query Object containing the query, e.g: { name: "Anna" } 
     * @param {String} collectionName
     */
    getDocumentsByQuery(query, collectionName) {
        if (!query || !collectionName) 
            return Promise.reject("Error: Query or collection name is empty");

        return this.db.collection(collectionName).find(query).toArray();
    }

    /**
     * Returns the first result on the db that matches the given query
     * @param {Object} query 
     * @param {String} collectionName 
     */
    getDocumentByQuery(query, collectionName) {
        if (!query || !collectionName) 
            return Promise.reject("Query or collection name is empty");

        return this.db.collection(collectionName).findOne(query);
    }

    /**
     * Updates a document in the collection and returns a promise with the result of the operation
     * @param {Object} document 
     * @param {Object} newValue
     * @param {String} collectionName
     */
    updateDocument(document, newValue, collectionName) {
        return this.db.collection(collectionName).updateOne( document, { $set: newValue });
    }

    /**
     * Deletes a document from the DB collection
     * @param {Object} document 
     * @param {String} collectionName
     */
    removeDocument(document, collectionName) {
        if (!document || !collectionName) {
            return Promise.reject("No document or collection given to remove");
        }
        return this.db.collection(collectionName).deleteOne(document);
    }

    /**
     * Deletes all the contents of a collection
     * @param {String} collectionName 
     */
    removeAllDocumentsFromCollection(collectionName) {
        return this.db.collection(collectionName).deleteMany( {} );
    }

    /**
     * Creates an index to improve the DB performance
     * @param {object} index  
     */
    indexCollection(index, collectionName) {
        return this.db.collection(collectionName).createIndex(
            index,
            null);
    }
}

module.exports = MongoDB;