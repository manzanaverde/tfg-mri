const express = require("express");
const session = require("express-session");
const fs = require('fs').promises;
const router = express.Router();
var multer = require('multer');

const DB = require("./DB.js");
const Patients = require("./modules/Patients.js").Patients;
const Users  = require("./modules/Users.js");
const Project = require("./modules/Project.js");
const FileSystem = require("./utils/fs.js"); 


var storage = multer.diskStorage( {
    destination: async function(req, file, cb) {
        let  url = `server/projects/${req.body.projectName}`;
        try {        
            const stat = await fs.stat(url);
            if (stat.isDirectory())
                cb(null, url);
            else 
                console.error("Stat is: " + stat);
        }
        catch(error) {
            if (error.code === "ENOENT")
                await fs.mkdir(url);
            else
                console.error(error);
        }
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname);
    }
});
var upload = multer( { storage: storage } );

class webServer {

    constructor(port) {

        this.port = port;
        this.upload = upload;

        // Mongo info
        this.mongoPort = 27017;
        this.dbName = "patientsDB";
        this.db = null;
        this.server = express();

        this.modules = [];

        //this.patients = new Patients(this);
        this.users = new Users(this);
        this.fs = new FileSystem(this);
        this.project = new Project(this);
        this.patients = new Patients(this);

        //this.registerModule(this.patients);
        this.registerModule(this.users);
        this.registerModule(this.project);
        this.registerModule(this.fs);
        this.registerModule(this.patients);
        
    }

    //waits for the modules to be started 
    async start() {
        return new Promise(async (resolve, reject) => {
            try {

                this.configureServer();
                
                // Connect once to the DB
                this.db = new DB(this.mongoPort, this.dbName);
                await this.db.connect();

                await this.initModules();
                this.initBinders();

                this.server.listen(this.port)
                    .on("listening", () => {
                        console.log(`Server listening at port: ${this.port}`);
                        resolve();
                    })
        
                    .on("error", err => reject(err));

            }
            catch(error) {
                reject(error);
            }
        });
    }

    configureServer() {

        // Configure CORS policy
        this.server.use( (req, res, next) => {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS");

            next();
        });

        // Serve static content
        this.server.use(express.static("www"));

        // Use sessions for login management
        this.server.use(session( {
            secret: 'Escandinavia is a wonderful place',
            resave: true,
            saveUninitialized: false
        }));

        // Init routes
        this.server.get('/', Users.requiresLogin, function(req,res) {
            res.sendFile(__dirname + '/index.html');
        });

        
    }

    initModules() {        
        try {
  
            for (let m = 0; m < this.modules.length; m++) {
                        
                if (this.modules[m].init)
                   this.modules[m].init();

            }
        }
        catch(error) {
            console.log(error);
        }
    }

    initBinders() {

        for (let d = 0; d < this.modules.length; d++) {

            if (this.modules[d].bindRequests)
                this.modules[d].bindRequests(this.server);
        }
    }

    registerModule(aModule) {
        this.modules.push(aModule);
    }

}

module.exports = webServer;