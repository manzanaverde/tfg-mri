class Core {
    constructor() {
        this.searchEngine = new SearchEngine(this);
        this.landingPage = new LandingPage(this);
        this.newScan = new NewScan(this);
        this.register = new Register(this);
        this.editor = new Editor(this);
        this.results = new Results(this);

        this.pages = [];
        this.volumetrics = null;

        this.addPage(this.searchEngine);
        this.addPage(this.landingPage);
        this.addPage(this.newScan);
        this.addPage(this.register);
        this.addPage(this.editor);
        this.addPage(this.results);
    }

    async init() {
        try {
            for (let i = 0; i < this.pages.length; i++) {
                let page = this.pages[i];

                if (page.requiresToLoadHTML)
                    page.html = await Network.loadHTMLFromFile(page.fileName);

                if (page.init)
                    page.init();
            }

            this.canvasContainer = document.createElement("div");
            this.canvasContainer.id = "canvasContainer";

            this.volumetrics = new Volumetrics({container: this.canvasContainer, visible: false, background: [0.3,0.3,0.3,1]});
            
            this.goToPage(this.landingPage);
        }
        catch(error) {
            console.error(error);
        }
    }

    addPage(page) {
        this.pages.push(page);
    }

    goToPage(page, param = null) {

        if (page)
            page.onEnter(param);
    }



    changeSiteHTML(html) {
        document.getElementById("site").innerHTML = html;
    }
}