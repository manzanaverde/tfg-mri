class LandingPage {
    
    constructor() {
        this.id = "landing";
    }

    onEnter() {
        this.attachListeners();
    }

    attachListeners() {
        document.getElementById("loginForm").addEventListener("submit", event => {
            event.preventDefault();
            login();
        });
    }


    //debug
    autoFill() {
        const form = document.getElementById("loginForm");
        form.email.value = "a@b.com";
        form.password.value = "adsfsdfsdf";
    }
    
}