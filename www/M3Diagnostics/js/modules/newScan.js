class NewScan {
    
    constructor() {
        this.id = "newScan";
    }

    onEnter() {
        this.attachListeners();

        const container = document.createElement("div");        // Dummy container
        this.volumetrics = new Volumetrics( { container: container, visible: true, background: [0.3,0.3,0.3,1] } );
    }

    attachListeners() {

        // Drag files zone
        document.getElementById("folderInput").addEventListener("change", this.onFolderUpload.bind(this), false);
        document.getElementById("newScanForm").addEventListener("submit", (event) => {
            event.preventDefault();
            this.uploadProject();
        });
    }

    onFolderUpload(event) {

        // Parse files
        if(event.target.files.length > 0)
            VolumeLoader.loadDicomFiles(event.target.files, this.onVolumeLoaded.bind(this), null);
    }
 
    async uploadProject() {
        try {

            // Get form data
            const form = document.getElementById("newScanForm");
            const data = new FormData(form);

            // Generate a single .vl light archive based on all .dcm and send it
            const arrayBuffer = this.volume.getVolumeAsVLBuffer();

            const blob = new Blob([arrayBuffer],  { type: 'application/octet-stream' } );
            data.append("vol", blob, "data.vl");

            // URL to send the request to
            const url = "/project/create";
            await Network.postFormData(url, data);
        }
        catch(error) {
            console.error(error);
        }
    }
    
    onVolumeLoaded(response) {
        if (response.status == VolumeLoader.DONE) {
            this.volume = response.volume;
            this.volumetrics.addVolume(this.volume, "myVol");    
            let node = new VolumeNode();
            node.volume = "myVol";
            this.volumetrics.addVolumeNode(node, "myVolNode");
        }
    }
}