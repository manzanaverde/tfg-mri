class Results {

    constructor() {
        this.html = null;
        this.id = "results";
        this.ROUTES = {
            firstName: "/patients/get/byQuery",
            name: "/projects/get/byQuery",
            allProjects: "/projects/get/all",
            allPatients: "/patients/get/all"
        }
    }

    onEnter() {

        // Get url parameters
        const url = new URL(window.location.href);
        const userQuery = url.searchParams.get("userQuery");
        const searchBy = url.searchParams.get("searchBy");

        if (!userQuery || !searchBy) {
            console.error("User query or searchBy parameters not provided");
            return;
        }

        this.keepUserQuery(userQuery, searchBy);
        this.searchByQuery(userQuery, searchBy);
    }
    
    attachListeners() {
    }

    async searchByQuery(userQuery, searchBy) {
        try {

            let url = this.ROUTES[searchBy];
            let projects = null;
            let patients = null;
            let data = null;

            switch (searchBy) {
                case "allPatients":
                    patients = await this.getAllPatients();
                    break;
                case "allProjects":
                    projects = await this.getAllProjects();
                    break;
                case "firstName":
                    data = new FormData();
                    data.append("userQuery", userQuery);
                    data.append("searchBy", searchBy);
                    patients = await Network.postFormData(this.ROUTES.firstName, data);
                    patients = JSON.parse(patients);
                    let orQuery = [];

                    // Build an OR query to equest every patient's info only once
                    for (let i = 0; i < patients.length; i++) {
                        let patient = patients[i];
                        orQuery.push( { patientId: patient.patientId } )
                    }

                    if (patients.length > 0) {
                        const query = { $or: orQuery };
                        let projectsData = new FormData();
                        projectsData.append("query", query);
                        projects = await Network.postFormData( this.ROUTES.name, projectsData);
                        projects = JSON.parse(projects);
                    }
                   
                    break;
                default:
                    console.error("unknown searchby type");
                    break;
            }
   
            this.showSearchResults(projects, patients);
        }
        catch(error) {
            console.error(error);
        }
    }

    noResults() {
        const container = document.getElementById("results");
        container.innerHTML = `<h4><i class="far fa-frown fa-3x"></i>Sorry, we didn't find what you were looking for...</h4>`;
    }

    keepUserQuery(userQuery, searchBy) {

        const form = document.getElementById("searchEngineForm");
        form.searchBy.value = searchBy;
        form.userQuery.value = userQuery;
    }

    showSearchResults(projects, patients) {

        if (projects.length == 0) {   // No results found
            this.noResults();
            return;
        }

        const tbody = document.getElementById("resultsTableBody");
        projects.forEach( project => {
            
            // Get corresponding patient
            let patient = patients.find( patient => patient.patientId === parseInt(project.patientId));

            if (!patient) {
                console.error("No patients found associated to the given projects");
                return;
            }
            
            let age = this.getCurrentAge(patient.birthDay);
            let scanDate = this.parseDate(project.creationTs);
            let tr = document.createElement("tr");
            tr.id = project._id;
            tr.classList = "cursor-pointer";
            tr.onclick = this.openEditor.bind(this, project._id, patient.patientId );
            tr.innerHTML = `<td><i class="material-icons md-48">face</i></td>
                    <td>${patient.firstName} ${patient.lastName}</td>
                    <td>${age}</td>
                    <td>${patient.phone}</td>
                    <td>${scanDate}</td>`;

            tbody.appendChild(tr);
        });
    }

    // timestamp
    getCurrentAge(birthday) {
        let now = new Date().getTime();     // to ms
        let diff = now - birthday;
        let age = Math.floor(diff/(1000*60*60*24*365.25));
        return age;
    }

    parseDate(date) {
        let d = new Date(date);
        let day = d.getDate();
        let month = d.getMonth() + 1;
        let year = d.getFullYear();
        let h = d.getHours();
        let m = d.getMinutes();

        return `${month}-${day}-${year} ${h}:${m}`;
    }

    openEditor(projectId, patientId) {
        window.location.href = `editor.html?projectId=${projectId}&patientId=${patientId}`;
    }

}
