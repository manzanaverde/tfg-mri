class Editor {

    constructor() {
        this.html = null;
        this.id = "editor";

        this.volumetrics = null;
        this.tfeditor = null;
        this.tool = null;

        this.downpick = null;
        this.movepick = null;
        this.labelNode = null;
        this.uppick = null;

        /* labels (annotations)
        position
        pointerposition
        text
        */

        /* fov
        target
        posicio
        */
    }

    onEnter() {

        const container = document.getElementById("canvasContainer");
        const tfContainer = document.getElementById("tfContainer");
        this.volumetrics = new Volumetrics( { container: container, visible: true, background: [0.3,0.3,0.3,1] } );
        this.tfeditor = new TFEditor( { container: tfContainer, visible: true } );
        this.tfeditor.setTF(this.volumetrics.getTransferFunction("tf_default"));

        this.volumetrics.setPickPositionCallback(this.onMouse.bind(this));
        this.volumetrics.labelCallback = this.onLabelInfo.bind(this);

        this.volumetrics.show();
        this.fillContent();
        this.attachListeners();

        // Get URL params
        const url = new URL(window.location.href);
        const projectId = url.searchParams.get("projectId");
        const patientId = url.searchParams.get("patientId");
        const viewId = url.searchParams.get("viewId");

        if (!projectId) {
            this.loadDefaultProject();
            console.log("Default project has been loaded");
            return;
        }

        this.loadProject(projectId, patientId, viewId);
    }

    fillContent() {

        // Color maps dropdown (color maps tab)
        const tfs = this.volumetrics.tfs;
        const list = document.getElementById("colorMapsList");
        for (let [key, value] of Object.entries(tfs)) {
            list.options.add(new Option(key, key));
        }

        // Color maps (views)
        const dropdown = document.getElementById("colorMap");
        for (let [key, value] of Object.entries(tfs)) {
            dropdown.options.add(new Option(key, key));
        }

    }

    attachListeners() {

        // Views tab
        document.getElementById("viewsList").addEventListener("change", this.switchToViewByName.bind(this));

        // Color maps tab
        document.getElementById("colorMapsList").addEventListener("change", this.updateCurrentTf);

        /*
        document.getElementById("cloneMap").addEventListener("click", this.cloneMap);
        document.getElementById("editMap").addEventListener("click", this.editMap);
        document.getElementById("deleteMap").addEventListener("click", this.deleteMap);*/

        // Editor tools
        // Camera
        document.getElementById("cameraZoom").addEventListener("click", this.changeCameraMode.bind(this,  Volumetrics.MODES.CAMERAZOOM));
        document.getElementById("cameraOrbit").addEventListener("click", this.changeCameraMode.bind(this, Volumetrics.MODES.CAMERAORBIT));
        document.getElementById("cameraRotate").addEventListener("click", this.changeCameraMode.bind(this, Volumetrics.MODES.CAMERAROTATE));
        document.getElementById("cameraReset").addEventListener("click", this.volumetrics.initCamera.bind(this, undefined, undefined, undefined));


        document.getElementById("fullscreen").addEventListener("click", this.toggleFullscreen);


        document.getElementById("text").addEventListener("click", () => {
            this.tool = "text";
            this.volumetrics.activeMode = Volumetrics.MODES.PICKPOSITION;
        });

        document.getElementById("removeText").addEventListener("click", () => {
            this.tool = "removeAnnotation";
            this.volumetrics.activeMode = Volumetrics.MODES.PICKPOSITION;
        });
    }

    changeCameraMode(cameraMode) {
        this.volumetrics.activeMode = cameraMode;

        const activeBtn = document.querySelector("#leftTools .text-primary");
        activeBtn.classList.toggle("text-primary");

        event.target.classList.toggle("text-primary");
    }

    onLabelInfo(info){
        if(info.click){
            if(this.tool == "removeAnnotation"){
                this.volumetrics.removeLabelNode(info.uid);
            }
        }else if(info.input){
            //console.log("Text changed! ", info.labelNode.text);
        }
    }

    goBack() {
        window.history.back();
    }

    async loadProject(projectId, patientId, viewId = null) {
        try {

            // Fetch volume and scene.json from DB
            const sceneUrl = `/project/get/scene`;
            const volumeUrl = `/project/get/volume`;

            this.projectId = projectId;
            let projectFolder = await this.getProjectFolder(projectId);
            projectFolder = JSON.parse(projectFolder).folder;
            const data = new FormData();
            data.append("folder", projectFolder);

            let scene = Network.postFormData(sceneUrl, data);
            this.scene = JSON.parse(await scene);
            const volumeBuffer = Network.getProjectFilesAsArrayBuffer(volumeUrl, data);

            // Get the patient associated to the project
            let patient = await this.getPatientInfo(patientId);
            patient = JSON.parse(patient);
            this.updatePatientInfo(patient);
            this.displayVolume(await volumeBuffer);
            this.updateSidebar(this.scene);

            if (viewId) {
                const view = this.getViewFromId(viewId);

                if (!view)          // View not found
                    view = -1;

                this.switchToView(view);
            }
            
        }
        catch(error) {
            console.error(error);
        }
    }

    getViewFromId(viewId) {
        for (let [key, value] of Object.entries(this.scene.views)) {
            if (value.id == viewId)
                return key;
        }
        return null;
    }

    loadDefaultProject() {
        this.loadProject("5d123ae8cc163d144051654f", "1");
    }

    getPatientInfo(patientId) {
        const data = new FormData();
        data.append("patientId", patientId);
        return Network.postFormData("/patient/get/byId", data);
    }

    getProjectFolder(projectId) {
        const data = new FormData();
        data.append("projectId", projectId);
        return Network.postFormData("/project/get/byId", data);
    }

    displayVolume(volumeBuffer) {
        VolumeLoader.parseVLBuffers([volumeBuffer], this.onVolumeLoaded.bind(this), null);
    }

    onVolumeLoaded(response) {
        if (response.status == VolumeLoader.DONE) {
            this.volume = response.volume;
            this.volumetrics.addVolume(this.volume, "myVol");    
            let node = new VolumeNode();
            node.volume = "myVol";
            this.volumetrics.addVolumeNode(node, "myVolNode");
        }
    }

    updateSidebar(scene) {
        this.updateViews(scene.views);
        //this.showTransferEditor();
    }

    updatePatientInfo(patient) {
        document.getElementById("patientId").innerHTML = patient.patientId;
        document.getElementById("patientName").innerHTML = patient.firstName + " " + patient.lastName;
        document.getElementById("patientPhone").innerHTML = patient.phone;

        let medHtml = "";
        const medication = document.getElementById("patientMedication");
        patient.medication.forEach( medic => {
            medHtml += `<li>Name: ${medic.name}
                        <br>Dose: ${medic.dose}
                        <br>Init date: ${medic.initDate.split("T")[0]}
                        <br>End date: ${medic.endDate.split("T")[0]}
                        </li>`;
        });
        medication.innerHTML = medHtml;

        const allergies = document.getElementById("patientAllergies");
        let allHtml = "";
        patient.allergies.forEach( allergy => {
            allHtml += `<li>Subject: ${allergy.subject}
                        <br>Detected: ${allergy.detected.split("T")[0]}`;
        });
        allergies.innerHTML = allHtml;

        document.getElementById("contactName").innerHTML = patient.emergencyContact.firstName + " " + patient.emergencyContact.lastName;
        document.getElementById("contactPhone").innerHTML = patient.emergencyContact.phone;
        document.getElementById("contactRelation").innerHTML = patient.emergencyContact.relation;
    }

    /**
     * 
     * @param {Object} views Object representing views
     */
    updateViews() {
        
        const select = document.getElementById("viewsList");
        select.options.length = 0;      // Remove old elements

        for (let [key, value] of Object.entries(this.scene.views)) {
            select.options.add(new Option(key, key, value.default));
        }
    }

    createNewView(event) {
        event.preventDefault();
        const name = event.target[0].value;
        
        this.scene.views[name] = {
            camera: this.getCurrentCamera(),
            transferFunction: this.getCurrentColorMap(),
            annotations: this.getCurrentAnnotations()
        };

        document.getElementById("newView").click();
        this.updateProjectScene();
        this.updateViews();
    }

    getCurrentCamera() {
        return {
            fov: this.volumetrics.camera.fov,
            target: this.volumetrics.camera.target.slice(0),
            position: this.volumetrics.camera.position.slice(0)
        }
    }

    getCurrentAnnotations() {
        let a = [];

        for (let [key, value] of Object.entries(this.volumetrics.labelNodes)) {
            a.push( {
                position: value.position,
                pointerPosition: value.pointerPosition,
                text: value.text
            });
        }

        return a;
    }

    getCurrentColorMap() {
        return document.getElementById("colorMap").value;
    }

    async updateProjectScene() {
        try {
            let url = "/project/update/scene";
            let data = new FormData();
            data.append("scene", JSON.stringify(this.scene));
            data.append("projectId", this.projectId);

            await Network.postFormData(url, data);
        }
        catch(error) {
            console.error(error);
        }
    }

    displayViewContent(event) {
        const viewName = event.target.value;
        const view = this.scene.views.find( view => view.name == viewName );
        let html = "<br>Annotations:<ul>";

        view.annotations.forEach( annotation => {
            html += `<li>${annotation}</li>`;
        });

        html += "</ul>";
        const container = document.getElementById("viewContent");
        container.innerHTML = html;
    }

    switchToView(view) {
        this.switchToTransferFunction(view.transferFunction);
        this.switchToCamera(view.camera);
        this.switchToAnnotations(view.annotations);
    }

    switchToViewByName(event) {
        const viewName = event.target.value;
        const view = this.scene.views[viewName];
        this.switchToView(view);
    }

    switchToCamera(camera) {
        this.volumetrics.initCamera(camera.fov, camera.position, camera.target);
    }

    showTransferEditor() {
        this.updateTfSelector();
    }
    
    onMouse(info) {

        if(info.down){
            this.downpick = info;
    
            if(this.downpick.left == true){
                if(this.tool == "text" && info.mouseGlobalPosition && this.labelNode == null) {
                    this.labelNode = new LabelNode();
                    this.labelNode.text = "...";
                    this.labelNode.pointerPosition = vec3.clone(info.mouseGlobalPosition);
                    this.labelNode.position = vec3.clone(info.mouseGlobalPosition);
                    this.volumetrics.addLabelNode(this.labelNode);
                }
            }
        }
        else if(info.dragging) {
            this.movepick = info;
    
            if(this.tool == "text" && this.labelNode != null){
                var delta = vec3.subtract(vec3.create(), this.movepick.mouseCameraPosition, this.downpick.mouseCameraPosition);
                this.labelNode.position = vec3.add(vec3.create(), this.labelNode.pointerPosition, delta);
            }
        }
        else if(info.up) {
            this.uppick = info;
    
            if(this.tool == "text" && this.labelNode != null){
                this.labelNode = null;
            }
    
            if(this.tool === "testPick" && info.mouseGlobalPosition != null){
                var sceneNode = new RD.SceneNode();
                sceneNode.mesh = "sphere";
                sceneNode.position = info.mouseGlobalPosition;
                sceneNode.color = [1, 1, 0];
    
                this.volumetrics.addSceneNode(sceneNode);
            }
        }
    }

    updateTfSelector(defaultOption = "tf_default") {
        const select = document.getElementById("colorMapsList");
        const tfs = this.volumetrics.getTransferFunctions();

        // Delete options
        for (let o = 0; o < select.options.length; o++) {
            select.remove(o);
        }

        for(let name of Object.keys(tfs)) {
            select.add( new Option(name, name, name == defaultOption) );
        }

        this.updateCurrentTf();
    }

    createNewMap(event) {
        event.preventDefault();
        const name = event.target[0].value;
        const tf = new TransferFunction();
        this.volumetrics.addTransferFunction(tf, name);
        this.updateTfSelector();
        document.getElementById("newMap").click();      // Close popup
    }

    updateMapName(event) {
        event.preventDefault();
        const newName = event.target[0].value;

        //???
    }

    cloneMap() {
        
        // Get values
        const params = {};
        params.name = document.getElementById("colorMapsList").value + "_1";
        params.r = document.getElementById("TFEditor_slider_r").value;
        params.g = document.getElementById("TFEditor_slider_g").value;
        params.b = document.getElementById("TFEditor_slider_b").value;
        params.a = document.getElementById("TFEditor_slider_a").value;
        this.createNewMap(params);
    }

    deleteColorMap(event) {
        event.preventDefault();

    }

    updateCurrentTf() {
        const current = document.getElementById("colorMapsList").value;
        this.volumetrics.getVolumeNode("myVolNode").tf = current;
        this.tfeditor.setTF(this.volumetrics.getTransferFunction(current));
    }

    switchToTransferFunction(transferFunction) {
        this.volumetrics.tf = transferFunction;
    }

    switchToAnnotations(annotations) {

        // Delete old ones
        this.volumetrics.labelNodes = {};

        // Add new ones
        for (let [key, value] of Object.entries(annotations)) {
            let labelNode = new LabelNode();
            labelNode.position = value.position;
            labelNode.pointerPosition = value.pointerPosition;
            labelNode.text = value.text;
            this.volumetrics.addLabelNode(labelNode);
        }
    }

    deleteView(event) {
        event.preventDefault();
        const select = document.getElementById("viewsList");

        if (select.selectedIndex == -1)
            return;

        const selectedName = select.value;
        
        delete this.scene.views[selectedName];
        console.log(`${selectedName} view has been deleted`);
        document.getElementById("deleteView").click();      // Close popover

        this.updateViews();         // Refresh dropwdown
    }

    toggleFullscreen() {
        try {
            const editor = document.getElementsByTagName("main")[0];

            if (!document.fullscreenElement) {      // There isn't any element on fullscreen mode
                editor.requestFullscreen();
            } else {
              if (editor.exitFullscreen) {
                editor.exitFullscreen(); 
              }
            }
        }
        catch(error) {
            console.error(error);
        }
    }
}
