class Register {

    constructor() {
        this.id = "register";
    }

    onEnter() {
        this.attachListeners();
    }

    attachListeners() {
        document.getElementById("registerForm").addEventListener("submit", event => {
            event.preventDefault();
            this.createAccount();
        });

        document.getElementById("loginForm").addEventListener("submit", login);
    }

    initModal() {
        $("#reusableModal").modal();
    }

    openModal(title, body, footer = null) {

        document.getElementsByClassName("modal-title")[0].innerHTML = title;
        document.getElementsByClassName("modal-body")[0].firstElementChild.innerHTML = body;
       
        if (footer) {
            document.getElementsByClassName("modal-footer")[0].innerHTML = footer;
        }

        $('#reusableModal').modal('show');
    }

    async createAccount() {
        try {
            const url = `/users/register`;
            const form = document.getElementById("registerForm");
            const data = new FormData(form);

            let res = await Network.postFormData(url, data);
            res = JSON.parse(res);
            
            if (res.code = 200) {
                this.resetForm();
                this.openModal("Registration successful", res.msg);
            }
            else
                this.openModal("Error during registration", res.msg)
        }
        catch(error) {
            console.error(error);
        }
    }

    resetForm() {
        document.getElementById("registerForm").reset();
    }

    //debug purposes
    autoFill() {
        const form = document.getElementById("registerForm");
        form.firstName.value = "anna";
        form.lastName.value = "b";
        form.organization.value = "upf";
        form.organizationCity.value = "bcn";
        form.job.value = "doctor";
        form.email.value = "random@gmail.com";
        form.password.value = "randomPwd";
        form.confirmPassword.value = "randomPwd";
    }
}