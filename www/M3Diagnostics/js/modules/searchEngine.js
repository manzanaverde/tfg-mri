class SearchEngine {

    constructor() {
        this.id = "searchEngine";
        this.SEARCH_TYPE = {
            patient: 0,
            project: 1 
        }
    }

    onEnter() {

        this.attachListeners();
        //this.fillForm();
    }

    attachListeners() {

        // Search by dropdown
        document.getElementById("searchBy").addEventListener("change", this.changeInputType.bind(this));
    }

    fillForm() {
        const form = document.getElementById("searchEngineForm");
        form.searchBy.value = "projectName";
        form.userQuery.value = "RandomName";
    }

    changeInputType(event) {

        const userQuery = document.getElementById("userQuery");
        let searchType = document.getElementById("searchType");

        switch (event.target.value) {
            case "firstName": 
                userQuery.type = "text";
                userQuery.disabled = false;
                userQuery.value = "";
                userQuery.placeholder = "Type the patient's name...";
                searchType.value = this.SEARCH_TYPE.patient;
                break;
            case "name":
                userQuery.type = "text";
                userQuery.disabled = false;
                userQuery.value = "";
                userQuery.placeholder = "Type the project's name...";
                searchType.value = this.SEARCH_TYPE.project;
                break;
            case "allProjects":
                userQuery.disabled = true;
                userQuery.value = "";
                searchType.value = this.SEARCH_TYPE.project;
                break;
            case "allPatients":
                userQuery.disabled = true;
                userQuery.value = "";
                searchType.value = this.SEARCH_TYPE.patient;
                break;
            default: 
                userQuery.type = "text"; 
                userQuery.value = "";
                userQuery.placeholder = "Select a filter from the list...";   
                break;
        }
    }    
}