class Network {

    static get(url, data = null) {
        return new Promise( (resolve, reject) => {
            const xhr = new XMLHttpRequest();
            xhr.open("GET", url);
            xhr.onload = () => resolve(xhr.responseText);
            xhr.onerror = () => reject(xhr.statusText);
            //xhr.setRequestHeader("Content-Type", "application/json");
            
            xhr.send(data);
        });
    }

    static post(url, data) {
        return new Promise( (resolve, reject) => {
            const xhr = new XMLHttpRequest();
            xhr.open("POST", url);
            xhr.onload = () => resolve(xhr.responseText);
            xhr.onerror = () => reject(xhr.statusText);
            xhr.setRequestHeader("Content-Type", "application/json");
            
            xhr.send(data);
        });
    }

    static postFormData(url, formData) {
        return new Promise( (resolve, reject) => {
            const xhr = new XMLHttpRequest();
            xhr.open("POST", url);
            xhr.onload = () => resolve(xhr.responseText);
            xhr.onerror = () => reject(xhr.statusText);
            xhr.send(formData);
        });
    }

    static getProjectFilesAsArrayBuffer(url, formData) {
        return new Promise( (resolve, reject) => {
            const xhr = new XMLHttpRequest();
            xhr.open("POST", url);
            xhr.onload = () => resolve(xhr.response);
            xhr.responseType = "arraybuffer";
            xhr.onerror = () => reject(xhr.statusText);
            xhr.send(formData);
        });
    }

    static loadHTMLFromFile(file) {
        return new Promise( (resolve, reject) => {
            const xhr = new XMLHttpRequest();
            xhr.open("GET", file);
            xhr.onload = () => resolve(xhr.response);
            xhr.onerror = () => reject(xhr.statusText);
            xhr.setRequestHeader("Content-Type", "text/html");
            xhr.setRequestHeader("Cache-Control", "no-cache");
            xhr.send();
        });
    }
}