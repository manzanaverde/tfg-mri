
async function login() {
    try {

        // Get form info and prepare data to send
        const form = document.getElementById("loginForm");
        const url = `/users/login?email=${form.loginEmail.value}&pass=${form.loginPassword.value}`;

        let user = await Network.get(url);
        user = JSON.parse(user);
        
        if (typeof(user) !== "object") {   // Something went wrong
            console.log(user);
            return;
        }

        // go to search engine
        window.location.href = `searchEngine.html`;

    }
    catch(error) {
        console.error(error);
    }
}